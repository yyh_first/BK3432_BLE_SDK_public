#<SYMDEFS># ARM Linker, 5050041: Last Updated: Fri Feb 28 18:30:29 2020

0x000012a1 T uart_stack_register

0x000012bd T Undefined_Exception
0x000012bf T SoftwareInterrupt_Exception
0x000012c1 T PrefetchAbort_Exception
0x000012c3 T DataAbort_Exception
0x000012c5 T Reserved_Exception
0x000012d1 T stack_main

0x000015e9 T ke_init
0x00001615 T ke_flush
0x00001655 T ke_sleep_check
0x0000166d T ke_event_init
0x00001679 T ke_event_callback_set
0x0000168d T ke_event_set
0x000016bf T ke_event_clear
0x000016f1 T ke_event_get
0x00001725 T ke_event_get_all
0x0000172b T ke_event_flush
0x00001733 T ke_event_schedule
0x0000175d T ke_mem_init
0x000017bd T ke_mem_is_empty
0x00001801 T ke_check_malloc
0x00001891 T ke_malloc
0x0000195b T ke_free
0x00001a2f T ke_is_free
0x00001a41 T ke_get_mem_usage
0x00001a45 T ke_get_max_mem_usage
0x00001a61 T ke_msg_alloc
0x00001a93 T ke_msg_send
0x00001ac5 T ke_msg_send_basic
0x00001ad3 T ke_msg_forward
0x00001add T ke_msg_forward_new_id
0x00001aed T ke_msg_free
0x00001af5 T ke_msg_dest_id_get
0x00001afb T ke_msg_src_id_get
0x00001b01 T ke_msg_in_queue
0x00001b11 T ke_queue_extract
0x00001b4f T ke_queue_insert
0x00001c89 T ke_task_init
0x00001c9d T ke_task_create
0x00001cdf T ke_task_delete
0x00001d15 T ke_state_set
0x00001d7f T ke_state_get
0x00001d9f T ke_msg_discard
0x00001da3 T ke_msg_save
0x00001da7 T ke_task_msg_flush
0x00001de9 T ke_task_check
0x00001ecd T ke_timer_init
0x00001ed9 T ke_timer_set
0x00001f6d T ke_timer_clear
0x00001fc3 T ke_timer_active
0x00001fe9 T ke_timer_adjust_all
0x00001ffd T ke_timer_target_get
0x0000200f T ke_timer_sleep_check
0x000020b1 T dbg_platform_reset_complete
0x00001351 T nvds_init
0x000013a5 T nvds_get
0x000020ed T co_list_init
0x000020f5 T co_list_push_back
0x0000210b T co_list_pool_init
0x00002167 T co_list_push_front
0x00002175 T co_list_pop_front
0x00002189 T co_list_extract
0x000021c5 T co_list_extract_after
0x000021df T co_list_find
0x000021f5 T co_list_merge
0x0000220f T co_list_insert_before
0x00002239 T co_list_insert_after
0x00002267 T co_list_size
0x0000227d T co_list_check_size_available
0x00002299 T co_bdaddr_compare
0x000022b9 T em_buf_init
0x00002389 T em_buf_rx_free
0x0000239b T em_buf_rx_buff_addr_get
0x000023ab T em_buf_tx_buff_addr_get
0x000023b3 T em_buf_tx_free
0x00002445 T llc_init
0x0000246f T llc_stop
0x000024b9 T llc_reset
0x000024dd T llc_le_con_cmp_evt_send
0x000026fd T llc_start
0x000027df T llc_discon_event_complete_send
0x00002801 T llc_con_update_complete_send
0x0000287d T llc_ltk_req_send
0x000028b3 T llc_feats_rd_event_send
0x000028ed T llc_version_rd_event_send
0x0000291f T llc_common_cmd_complete_send
0x0000293d T llc_common_cmd_status_send
0x00002959 T llc_common_flush_occurred_send
0x00002973 T llc_common_enc_key_ref_comp_evt_send
0x00002991 T llc_common_enc_change_evt_send
0x000029fb T llc_common_nb_of_pkt_comp_evt_send
0x00002a1d T llc_con_update_ind
0x00002a59 T llc_lsto_con_update
0x00002a8b T llc_map_update_ind
0x00002adb T llc_con_update_finished
0x00002b5f T llc_map_update_finished
0x000034db T llc_hci_command_handler
0x00003509 T llc_hci_acl_data_tx_handler
0x0000360b T llc_llcp_version_ind_pdu_send
0x00003629 T llc_llcp_ch_map_update_pdu_send
0x00003665 T llc_llcp_pause_enc_req_pdu_send
0x00003693 T llc_llcp_pause_enc_rsp_pdu_send
0x000036c9 T llc_llcp_enc_req_pdu_send
0x0000379b T llc_llcp_enc_rsp_pdu_send
0x00003811 T llc_llcp_start_enc_rsp_pdu_send
0x00003859 T llc_llcp_reject_ind_pdu_send
0x000038a9 T llc_llcp_con_update_pdu_send
0x000038d5 T llc_llcp_con_param_req_pdu_send
0x0000391d T llc_llcp_con_param_rsp_pdu_send
0x00003963 T llc_llcp_feats_req_pdu_send
0x0000398f T llc_llcp_feats_rsp_pdu_send
0x000039bd T llc_llcp_start_enc_req_pdu_send
0x00003a07 T llc_llcp_terminate_ind_pdu_send
0x00003a6b T llc_llcp_unknown_rsp_send_pdu
0x00003a81 T llc_llcp_ping_req_pdu_send
0x00003a93 T llc_llcp_ping_rsp_pdu_send
0x00003aa5 T llc_llcp_length_req_pdu_send
0x00003acf T llc_llcp_length_rsp_pdu_send
0x00003c71 T llc_llcp_recv_handler
0x00004e0b T llc_llcp_get_autorize
0x000055a1 T llc_util_get_free_conhdl
0x000055c9 T llc_util_get_nb_active_link
0x000055f1 T llc_util_dicon_procedure
0x00005631 T llc_util_update_channel_map
0x00005643 T llc_util_set_llcp_discard_enable
0x0000565b T llc_util_set_auth_payl_to_margin
0x00005687 T llc_util_clear_operation_ptr
0x000056a3 T llc_util_bw_mgt
0x000056f5 T llc_end_evt_defer
0x0000573b T llc_pdu_llcp_tx_ack_defer
0x0000582f T llc_pdu_acl_tx_ack_defer
0x00005851 T llc_pdu_defer
0x000058fd T lld_init
0x00005a55 T lld_core_reset
0x00005c79 T lld_con_update_after_param_req
0x00005ee5 T lld_con_param_rsp
0x00005fd7 T lld_con_param_req
0x00006095 T lld_con_stop
0x000060a1 T lld_get_mode
0x00006239 T lld_crypt_isr
0x00006243 T lld_ral_renew_req
0x00006299 T lld_evt_elt_delete
0x0000650b T lld_evt_drift_compute
0x00006639 T lld_evt_delete_elt_push
0x00006829 T lld_evt_channel_next
0x00006863 T lld_evt_deffered_elt_handler
0x00006933 T lld_evt_init
0x000069b5 T lld_evt_init_evt
0x00006a07 T lld_evt_restart
0x00006d65 T lld_evt_elt_insert
0x00006f6f T lld_evt_schedule
0x0000703d T lld_evt_prevent_stop
0x0000703f T lld_evt_canceled
0x00007423 T lld_evt_adv_create
0x00007493 T lld_evt_end
0x000075d5 T lld_evt_rx
0x00007611 T lld_evt_timer_isr
0x0000761b T lld_evt_end_isr
0x00007651 T lld_evt_rx_isr
0x00007749 T lld_pdu_data_tx_push
0x00007bf9 T lld_pdu_check
0x00007dc1 T lld_pdu_tx_loop
0x00007e03 T lld_pdu_data_send
0x00007e5b T lld_pdu_tx_push
0x00007eb7 T lld_pdu_tx_prog
0x00008229 T lld_pdu_tx_flush
0x00008299 T lld_pdu_adv_pack
0x000082bd T lld_pdu_rx_handler
0x000084b9 T lld_sleep_init
0x000084ff T lld_sleep_enter
0x0000853f T lld_sleep_wakeup
0x00008559 T lld_sleep_wakeup_end
0x000085d5 T lld_util_instant_get
0x000085ed T lld_util_get_bd_address
0x0000860d T lld_util_set_bd_address
0x0000864b T lld_util_ral_force_rpa_renew
0x0000868b T lld_util_freq2chnl
0x000086af T lld_util_get_local_offset
0x000086cd T lld_util_get_peer_offset
0x000086ed T lld_util_connection_param_set
0x00008737 T lld_util_dle_set_cs_fields
0x00008759 T lld_util_anchor_point_move
0x00008829 T lld_util_flush_list
0x0000883f T lld_util_instant_ongoing
0x0000884b T lld_util_compute_ce_max
0x000088dd T lld_util_elt_programmed
0x000088fb T lld_util_priority_set
0x0000893f T lld_util_priority_update
0x00008945 T lld_util_get_tx_pkt_cnt
0x0000894b T lld_util_eff_tx_time_set
0x00008971 T llm_ral_clear
0x0000898f T llm_wl_clr
0x000089e1 T llm_encryption_start
0x00008a6f T llm_encryption_done
0x00008b07 T llm_init
0x00008c05 T llm_common_cmd_complete_send
0x00008c1d T llm_ble_ready
0x00008d8d T llm_common_cmd_status_send
0x00008f0d T llm_ral_update
0x00009153 T llm_wl_dev_add
0x000091ad T llm_wl_dev_rem
0x000091d3 T llm_wl_dev_add_hdl
0x00009245 T llm_wl_dev_rem_hdl
0x000092af T llm_ral_dev_add
0x000093ad T llm_ral_dev_rm
0x000093e3 T llm_ral_get_rpa
0x0000945d T llm_ral_set_timeout
0x00009909 T hci_command_handler
0x000099e9 T llm_util_bd_addr_wl_position
0x00009a5b T llm_util_bl_check
0x00009ac5 T llm_util_bd_addr_in_wl
0x00009b13 T llm_util_check_address_validity
0x00009b23 T llm_util_check_map_validity
0x00009b73 T llm_util_apply_bd_addr
0x00009b91 T llm_util_set_public_addr
0x00009ba1 T llm_util_check_evt_mask
0x00009bc1 T llm_util_get_channel_map
0x00009bc3 T llm_util_get_supp_features
0x00009c0b T llm_util_bd_addr_in_ral
0x00009c79 T llm_util_bl_add
0x00009cfd T llm_util_bl_rem
0x00009d5f T llm_end_evt_defer
0x00009e27 T llm_pdu_defer
0x00009f49 T ea_elt_cancel
0x0000a045 T ea_time_get_slot_rounded
0x0000a123 T ea_init
0x0000a147 T ea_elt_create
0x0000a161 T ea_time_get_halfslot_rounded
0x0000a18b T ea_elt_insert
0x0000a3d7 T ea_elt_remove
0x0000a441 T ea_interval_create
0x0000a457 T ea_interval_insert
0x0000a465 T ea_interval_remove
0x0000a475 T ea_interval_delete
0x0000a48f T ea_finetimer_isr
0x0000a563 T ea_sw_isr
0x0000a583 T ea_offset_req
0x0000a73b T ea_timer_target_get
0x0000a757 T ea_sleep_check
0x0000a7a5 T ea_interval_duration_req
0x0000bda9 T gapm_update_state
0x0000be0d T gapm_init
0x0000be6d T gapm_init_attr
0x0000be99 T gapm_get_operation
0x0000beab T gapm_get_requester
0x0000bec3 T gapm_reschedule_operation
0x0000bee5 T gapm_send_complete_evt
0x0000bf1b T gapm_send_error_evt
0x0000bf3b T gapm_get_address_type
0x0000bf47 T gapm_con_create
0x0000bfb5 T gapm_con_enable
0x0000bfc1 T gapm_con_cleanup
0x0000bff7 T gapm_get_id_from_task
0x0000c037 T gapm_get_task_from_id
0x0000c079 T gapm_is_disc_connection
0x0000c09f T gapm_get_max_mtu
0x0000c0a5 T gapm_get_max_mps
0x0000c0ab T gapm_set_max_mtu
0x0000c0c5 T gapm_set_max_mps
0x0000c0db T gapm_addr_check
0x0000c0f7 T gapm_dle_val_check
0x0000c113 T gapm_is_legacy_pairing_supp
0x0000c11f T gapm_is_sec_con_pairing_supp
0x0000c12b T gapm_is_pref_con_param_pres
0x0000c137 T gapm_get_att_handle
0x0000c15f T gapm_svc_chg_en
0x0000c45b T gapm_hci_handler
0x0000cc9d T gapm_get_local_addrtype
0x0000ce83 T gapm_get_role
0x0000ce89 T gapm_update_air_op_state
0x0000cf3d T gapm_get_irk
0x0000cf43 T gapm_get_bdaddr
0x0000cf49 T gapm_basic_hci_cmd_send
0x0000cf5d T gapm_setup_conn
0x0000cfb9 T gapm_get_adv_mode
0x0000cfd5 T gapm_op_reset_continue
0x0000d05b T gapm_op_setup_continue
0x0000a89b T gapc_update_state
0x0000a8cb T gapc_get_requester
0x0000a8e7 T gapc_send_complete_evt
0x0000a9b1 T gapc_init
0x0000a9e3 T gapc_con_create
0x0000aa83 T gapc_con_cleanup
0x0000aa93 T gapc_send_disconect_ind
0x0000aab5 T gapc_get_conidx
0x0000aadb T gapc_get_conhdl
0x0000aaef T gapc_get_role
0x0000ab0b T gapc_get_bdaddr
0x0000ab2b T gapc_get_csrk
0x0000ab2f T gapc_get_sign_counter
0x0000ab33 T gapc_send_error_evt
0x0000ab55 T gapc_get_operation
0x0000ab6b T gapc_get_operation_ptr
0x0000ab77 T gapc_set_operation_ptr
0x0000ab83 T gapc_reschedule_operation
0x0000abb3 T gapc_is_sec_set
0x0000abe7 T gapc_lk_sec_lvl_get
0x0000ac01 T gapc_enc_keysize_get
0x0000ac05 T gapc_enc_keysize_set
0x0000ac07 T gapc_link_encrypted
0x0000ac1f T gapc_auth_set
0x0000ac79 T gapc_auth_get
0x0000aca9 T gapc_svc_chg_ccc_get
0x0000acb9 T gapc_svc_chg_ccc_set
0x0000accf T gapc_param_update_sanity
0x0000b247 T gapc_hci_handler
0x0000b351 T gapc_sig_pdu_recv_handler
0x0000b38d T gapc_process_op
0x0000fbd9 T gattm_svc_get_start_hdl
0x0000fbdf T gattm_init
0x0000fbfd T gattm_init_attr
0x0000fc65 T gattm_create
0x0000fc6d T gattm_cleanup
0x0000f3d7 T gattc_update_state
0x0000f3fb T gattc_get_requester
0x0000f417 T gattc_send_complete_evt
0x0000f475 T gattc_cleanup
0x0000f4ef T gattc_init
0x0000f523 T gattc_create
0x0000f57b T gattc_con_enable
0x0000f58f T gattc_get_mtu
0x0000f599 T gattc_set_mtu
0x0000f5d3 T gattc_send_error_evt
0x0000f5f9 T gattc_get_operation
0x0000f60f T gattc_get_op_seq_num
0x0000f625 T gattc_get_operation_ptr
0x0000f631 T gattc_set_operation_ptr
0x0000f63d T gattc_reschedule_operation
0x0000d1f1 T attc_l2cc_pdu_recv_handler
0x0000d251 T attm_convert_to128
0x0000d281 T attm_uuid_comp
0x0000d2d9 T attm_uuid16_comp
0x0000d2e5 T attm_is_bt16_uuid
0x0000d30b T attm_is_bt32_uuid
0x0000d333 T attm_convert_to128_ti
0x0000d363 T attm_svc_create_db
0x0000d463 T attm_svc_create_db_128
0x0000d58f T attm_svc_create_db128
0x0000d6cd T attm_reserve_handle_range
0x0000d6ef T attm_att_set_value
0x0000d7a1 T attm_get_value
0x0000d909 T attm_att_set_permission
0x0000d97f T attm_att_update_perm
0x0000d9ed T attm_svc_set_permission
0x0000da11 T attm_svc_get_permission
0x0000da2f T attm_init
0x0000dd47 T attmdb_svc_check_hdl
0x0000dd97 T attmdb_add_service
0x0000de0d T attmdb_destroy
0x0000de27 T attmdb_get_service
0x0000de65 T attmdb_get_attribute
0x0000de99 T attmdb_get_next_att
0x0000df0f T attmdb_uuid16_comp
0x0000df4d T attmdb_get_max_len
0x0000dfb7 T attmdb_get_uuid
0x0000e08d T attmdb_att_get_permission
0x0000e23d T atts_clear_read_cache
0x0000e397 T atts_send_error
0x0000e3bb T atts_write_signed_cfm
0x0000e3fd T atts_send_event
0x0000e489 T atts_clear_prep_data
0x0000e4a7 T atts_clear_rsp_data
0x0000e4c3 T atts_write_rsp_send
0x0000f15f T atts_process_pdu
0x0000f1c5 T atts_l2cc_pdu_recv_handler
0x0000fc89 T l2cc_send_error_evt
0x0000fcb1 T l2cc_update_state
0x0000fcd7 T l2cc_send_complete_evt
0x0000fd29 T l2cc_cleanup
0x0000fdc5 T l2cc_init
0x0000fdfb T l2cc_create
0x0000fe35 T l2cc_get_operation
0x0000fe49 T l2cc_get_operation_ptr
0x0000fe55 T l2cc_set_operation_ptr
0x0000fe61 T l2cc_data_send
0x0000ffc5 T l2cc_pdu_pack
0x00010235 T l2cc_pdu_unpack
0x0001049b T l2cc_pdu_header_check
0x00010579 T l2cc_sig_send_cmd_reject
0x00010635 T l2cc_sig_pdu_recv_handler
0x00010681 T l2cc_process_op
0x000108fb T l2cc_pdu_alloc
0x00010925 T l2cc_pdu_send
0x00010987 T l2cm_init
0x000109ad T l2cm_create
0x000109b5 T l2cm_cleanup
0x000109bf T l2cm_set_link_layer_buff_size
0x000109c9 T l2cm_get_nb_buffer_available
0x000109d5 T l2cm_tx_status
0x00010a09 T hci_evt_mask_set
0x00010a53 T hci_init
0x00010a6f T hci_reset
0x00010a8b T hci_send_2_host
0x00010b13 T hci_send_2_controller
0x00010bd5 T hci_fc_init
0x00010be1 T hci_fc_acl_buf_size_set
0x00010c01 T hci_fc_acl_en
0x00010c29 T hci_fc_acl_packet_sent
0x00010c39 T hci_fc_host_nb_acl_pkts_complete
0x00010c4b T hci_fc_check_host_available_nb_acl_packets
0x00010c75 T hci_look_for_cmd_desc
0x00010cc5 T hci_look_for_evt_desc
0x00010ceb T hci_look_for_le_evt_desc
0x00010d15 T __aeabi_uidiv
0x00010d15 T __aeabi_uidivmod
0x00010d41 T __aeabi_idiv
0x00010d41 T __aeabi_idivmod
0x00010d69 T rand
0x00010d7b T srand
0x00010d8d T __aeabi_memcpy
0x00010d8d T __aeabi_memcpy4
0x00010d8d T __aeabi_memcpy8
0x00010db1 T __aeabi_memset
0x00010db1 T __aeabi_memset4
0x00010db1 T __aeabi_memset8
0x00010dc1 T __aeabi_memclr
0x00010dc1 T __aeabi_memclr4
0x00010dc1 T __aeabi_memclr8
0x00010dc5 T _memset$wrapper
0x00010dd7 T memcmp
0x00010df0 A __ARM_clz
0x00010df8 A __ARM_common_disable_fiq
0x00010e0c A __ARM_common_disable_irq
0x00010e20 A __ARM_common_enable_fiq
0x00010e30 A __ARM_common_enable_irq
0x00010e40 A __ARM_common_switch8
0x00010e5c D one_bits
0x00010e6c D co_sca2ppm
0x00010e7c D co_null_bdaddr
0x00011034 D llc_default_handler
0x0001104c D rwip_coex_cfg
0x00011051 D rwip_priority
0x00011060 D lld_pdu_adv_pk_desc_tab
0x000110b4 D lld_pdu_llcp_pk_desc_tab
0x000111c2 D llm_local_data_len_values
0x000111e0 D llm_local_cmds
0x00011220 D llm_local_le_feats
0x00011228 D llm_local_le_states
0x00011300 D llm_default_handler
0x00011308 D LLM_AA_CT1
0x0001130b D LLM_AA_CT2
0x00011590 D gapm_default_state
0x00011618 D gapm_default_handler
0x000113c8 D gapc_sig_handlers
0x000113e0 D gapc_default_state
0x00011490 D gapc_default_handler
0x00011768 D gattm_default_state
0x00011770 D gattm_default_handler
0x000116bc D gattc_default_state
0x0001172c D gattc_default_handler
0x00011644 D atts_handlers
0x0001189c D l2cc_sig_handlers
0x000118b4 D l2cc_default_state
0x000118ec D l2cc_default_handler
0x00011904 D hci_cmd_desc_tab_lk_ctrl
0x00011910 D hci_cmd_desc_tab_ctrl_bb
0x00011938 D hci_cmd_desc_tab_info_par
0x0001194c D hci_cmd_desc_tab_stat_par
0x00011950 D hci_cmd_desc_tab_le
0x00011a08 D hci_cmd_desc_tab_vs
0x00011a14 D hci_cmd_desc_root_tab
0x00011a44 D hci_evt_desc_tab
0x00011a56 D hci_evt_le_desc_tab
0x00400024 D co_default_bdaddr
0x0040002c D llc_env
0x00400030 D llc_state
0x00400031 D latency_disable_state
0x00400032 D system_sleep_flag
0x00400034 D lld_sleep_env
0x00400038 D llm_state
0x00400041 D gapm_state
0x0040003c D gapc_env
0x00400040 D gapc_state
0x00400049 D gattm_state
0x00400044 D gattc_env
0x00400048 D gattc_state
0x0040004c D l2cc_env
0x00400050 D l2cc_state
0x0040007c D ke_env
0x0040011c D rwip_rf
0x00400150 D rom_env
0x0040019c D em_buf_env
0x004004e4 D lld_evt_env
0x00400510 D llm_le_env
0x0040058c D gapm_env
0x004005b8 D gattm_env
0x004005d8 D l2cm_env
0x004005e4 D hci_env
0x004005f4 D hci_fc_env
